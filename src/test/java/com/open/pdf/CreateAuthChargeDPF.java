package com.open.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.open.pdf.openpdf.Liberation;
import com.open.pdf.openpdf.PageXofY;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.*;
import java.io.FileOutputStream;

@SpringBootTest
public class CreateAuthChargeDPF {
    @Test
    public void createApproveChargePDF() {

        System.out.println("document.add(chineseTable)");
        // step1
        Document document = new Document(PageSize.A4.rotate(), 25, 25, 25, 36);
        try {
            // step2
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream("d:/testPDF/approveChargeBill.pdf"));

            final Font heardFont = Liberation.CH_CN.create(30, Font.BOLD);
            final Font titleFont = Liberation.CH_CN.create(16, Font.BOLD);
            final Font chineseFont = Liberation.CH_CN.create(10, Font.NORMAL);
            final Font chineseSmallFont = Liberation.CH_CN.create(8, Font.NORMAL);

            writer.setPageEvent(new PageXofY(Liberation.CH_CN.create().getBaseFont()));

            // step3
            document.open();
            //添加第一行头部
            Paragraph p = new Paragraph("费用账单审批详情", heardFont);
            p.setAlignment(Element.ALIGN_CENTER);
            document.add(p);
            document.add(new Paragraph(" "));

            //审批详情
            document.add(new Paragraph("审批详情", titleFont));
            document.add(new Paragraph(" "));
            PdfPTable approveTable = new PdfPTable(4);
            int[] approveWidth = {25, 25, 25, 25}; // 头部占比
            approveTable.setWidths(approveWidth);
            approveTable.setWidthPercentage(100); // percentage
            approveTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            approveTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            addCellContentDetail("审批类型：账单复核流程", 1, 5, chineseFont, approveTable);
            addCellContentDetail("审批状态：处理中", 1, 5, chineseFont, approveTable);
            addCellContentDetail("发起人：招商经理", 1, 5, chineseFont, approveTable);
            addCellContentDetail("发起时间：2022-09-29 22:01:35", 1, 5, chineseFont, approveTable);
            document.add(approveTable);

            //流程轨迹
            document.add(new Paragraph(" "));
            document.add(new Paragraph("流程轨迹", titleFont));
            document.add(new Paragraph(" "));
            PdfPTable approveLogTable = new PdfPTable(5);
            int[] approveLogWidth = {20, 20, 20, 20, 20}; // 头部占比
            approveLogTable.setWidths(approveLogWidth);
            approveLogTable.setWidthPercentage(100); // percentage
            approveLogTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            approveLogTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            addCellContent("当前环节", 1, 5, chineseFont, approveLogTable);
            addCellContent("审批人", 1, 5, chineseFont, approveLogTable);
            addCellContent("审批状态", 1, 5, chineseFont, approveLogTable);
            addCellContent("审批意见", 1, 5, chineseFont, approveLogTable);
            addCellContent("审批时间", 1, 5, chineseFont, approveLogTable);
            //添加流程轨迹内容
            for (int i = 0; i < 5; i++) {
                addCellContent("创建", 1, 5, chineseFont, approveLogTable);
                addCellContent("张三", 1, 5, chineseFont, approveLogTable);
                addCellContent("审批中", 1, 5, chineseFont, approveLogTable);
                addCellContent("同意", 1, 5, chineseFont, approveLogTable);
                addCellContent("2022-09-29 22:01:35", 1, 5, chineseFont, approveLogTable);
            }
            document.add(approveLogTable);

            //账单详情
            document.add(new Paragraph(" "));
            document.add(new Paragraph("账单详情", titleFont));
            document.add(new Paragraph(" "));
            PdfPTable chargeTable = new PdfPTable(16);
            int[] chargeWidth = {3, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7}; // 头部占比
            chargeTable.setWidths(chargeWidth);
            chargeTable.setWidthPercentage(100); // percentage
            chargeTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            chargeTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            addCellContent("序号", 1, 5, chineseFont, chargeTable);
            addCellContent("企业名称", 1, 5, chineseFont, chargeTable);
            addCellContent("合同编号", 1, 5, chineseFont, chargeTable);
            addCellContent("收费主体", 1, 5, chineseFont, chargeTable);
            addCellContent("房间名称", 1, 5, chineseFont, chargeTable);
            addCellContent("应收押金", 1, 5, chineseFont, chargeTable);
            addCellContent("实收押金", 1, 5, chineseFont, chargeTable);
            addCellContent("租金", 1, 5, chineseFont, chargeTable);
            addCellContent("管理费", 1, 5, chineseFont, chargeTable);
            addCellContent("空调费", 1, 5, chineseFont, chargeTable);
            addCellContent("电费", 1, 5, chineseFont, chargeTable);
            addCellContent("水费", 1, 5, chineseFont, chargeTable);
            addCellContent("公摊水费", 1, 5, chineseFont, chargeTable);
            addCellContent("公摊电费", 1, 5, chineseFont, chargeTable);
            addCellContent("调整金额", 1, 5, chineseFont, chargeTable);
            addCellContent("应收合计", 1, 5, chineseFont, chargeTable);

            //添加账单详情内容 禁止触发超过50万条数据，会撑爆的
            for (int i = 1; i < 240000; i++) {
                addCellContent("" + i, 1, 3, chineseSmallFont, chargeTable);
                addCellContent("测试公司", 1, 5, chineseSmallFont, chargeTable);
                addCellContent("HT00000000102", 1, 5, chineseSmallFont, chargeTable);
                addCellContent("10-503,10-504,10-505", 1, 5, chineseSmallFont, chargeTable);
                addCellContent("208252.17 ", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("20852.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("20852.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("20852.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("20852.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("20852.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
                addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            }
            addCellContent("", 3, 3, chineseSmallFont, chargeTable);
            addCellContent("总计", 1, 5, chineseSmallFont, chargeTable);
            addCellContent("208252.17 ", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            addCellContent("208252.17", 1, 3, chineseSmallFont, chargeTable);
            chargeTable.setHeaderRows(1);//设置固定头部
            document.add(chargeTable);
        } catch (Exception de) {
            de.printStackTrace();
        }
        // step5
        document.close();
    }

    private void addCellContent(String content, int colSpan, int padding, Font chineseFont, PdfPTable datatable) {
        Paragraph p = new Paragraph(content, chineseFont);
        PdfPCell cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPadding(padding);
        cell.setColspan(colSpan);
        datatable.addCell(cell);
    }

    private void addCellContentLeft(String content, int colSpan, int padding, Font chineseFont, PdfPTable datatable) {
        Paragraph p = new Paragraph(content, chineseFont);
        PdfPCell cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPadding(padding);
        cell.setColspan(colSpan);
        datatable.addCell(cell);
    }

    private void addCellContentDetail(String content, int colSpan, int padding, Font chineseFont, PdfPTable datatable) {
        Paragraph p = new Paragraph(content, chineseFont);
        PdfPCell cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderColor(Color.white);
        cell.setPadding(padding);
        cell.setColspan(colSpan);
        datatable.addCell(cell);
    }
}
