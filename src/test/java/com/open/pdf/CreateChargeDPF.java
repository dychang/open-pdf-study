package com.open.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.open.pdf.openpdf.Liberation;
import com.open.pdf.openpdf.PageXofY;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.*;
import java.io.FileOutputStream;

@SpringBootTest
public class CreateChargeDPF {
    @Test
    public void createChargePDF() {

        System.out.println("document.add(chineseTable)");
        // step1
        Document document = new Document(PageSize.A4, 25, 25, 25, 36);
        try {
            // step2
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream("d:/testPDF/chargeBill.pdf"));

            final Font heardFont = Liberation.CH_CN.create(30, Font.BOLD);
            final Font chineseFont = Liberation.CH_CN.create(10, Font.NORMAL);

            writer.setPageEvent(new PageXofY(Liberation.CH_CN.create().getBaseFont()));

            // step3
            document.open();
            PdfPCell cell;
            //添加第一行头部
            Paragraph p = new Paragraph("前海深港创新中心收费通知单", heardFont);
            p.setAlignment(Element.ALIGN_CENTER);
            document.add(p);
            document.add(new Paragraph(" "));
            //添加头部信息
            PdfPTable heardTable = new PdfPTable(2);
            int[] headWitd = {75, 25}; // 头部占比
            heardTable.setWidths(headWitd);
            heardTable.setWidthPercentage(100); // percentage
            heardTable.getDefaultCell().setPadding(5);
            heardTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            heardTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            p = new Paragraph("大楼名称：D组团", chineseFont);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(Color.white);
            cell.setPaddingBottom(10);
            heardTable.addCell(cell);

            p = new Paragraph("制表日期：2022-09-25", chineseFont);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(Color.white);
            cell.setPaddingBottom(10);
            heardTable.addCell(cell);
            document.add(heardTable);


            // 添加账单信息
            String[] bogusData = {"电费", "M0065920", "8029.0", "8096.0", "100.0",
                    "100.00", "6700.00", "1.05", "7035.00", "2022.8.1-2022.8.31",
                    "FLFLMTGP", "153", "中文查询\n11、\nabc\n中文测试红红火火恍恍惚惚或或或或或或"};

            int NumColumns = 10;
            PdfPTable datatable = new PdfPTable(NumColumns);
            int[] headerwidths = {4, 14, 10, 10, 7, 8, 10, 7, 11, 19}; // percentage
            datatable.setWidths(headerwidths);
            datatable.setWidthPercentage(100); // percentage
            datatable.getDefaultCell().setPadding(5);
            datatable.getDefaultCell().setHorizontalAlignment(
                    Element.ALIGN_CENTER);
            datatable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            //单元格跨页
            datatable.setSplitLate(false);
            datatable.setSplitRows(true);

            //添加公司和楼栋信息
            p = new Paragraph("房间代码：spom-2F-201,spom-3F-301,spom2-2F-201,spom2-3F-301", chineseFont);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderWidthRight(0);
            cell.setPadding(10);
            cell.setColspan(7);
            datatable.addCell(cell);
            p = new Paragraph("客户名称：XXXX", chineseFont);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderWidthLeft(0);
            cell.setPadding(10);
            cell.setColspan(3);
            datatable.addCell(cell);
            //内部表头
            addCellContent("项目名称", 2, 7, chineseFont, datatable);
            addCellContent("上次读数", 1, 7, chineseFont, datatable);
            addCellContent("本次读数", 1, 7, chineseFont, datatable);
            addCellContent("倍率", 1, 7, chineseFont, datatable);
            addCellContent("占比(%)", 1, 2, chineseFont, datatable);
            addCellContent("倍率", 1, 7, chineseFont, datatable);
            addCellContent("本次用量", 1, 7, chineseFont, datatable);
            addCellContent("应缴费用\n(单位:元)", 1, 7, chineseFont, datatable);
            addCellContent("收费期间", 1, 7, chineseFont, datatable);

            //合并 电费
            cell = new PdfPCell(new Paragraph("电\n费", chineseFont));
            cell.setRowspan(52);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            datatable.addCell(cell);
            //添加电费
            for (int i = 1; i < 53; i++) {
                for (int x = 1; x < NumColumns; x++) {
                    addCellContent(bogusData[x], 1, 5, chineseFont, datatable);
                }
            }
            //添加分摊电费
            addCellContent("分摊电费", 2, 5, chineseFont, datatable);
            addCellContentLeft("在楼层-2F收费面积占比为36%；", 4, 5, chineseFont, datatable);
            addCellContent("109.00", 1, 5, chineseFont, datatable);
            addCellContent("1.30", 1, 5, chineseFont, datatable);
            addCellContent("141.70", 1, 5, chineseFont, datatable);
            addCellContent("2022.7.1-2022.7.31", 1, 5, chineseFont, datatable);
            //本月应缴电费
            addCellContent("本月应缴电费", 8, 5, chineseFont, datatable);
            addCellContent("921.70", 1, 5, chineseFont, datatable);
            addCellContent(" ", 1, 5, chineseFont, datatable);


            //合并 水费
            cell = new PdfPCell(new Paragraph("水\n费", chineseFont));
            cell.setRowspan(49);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            datatable.addCell(cell);
            //添加水费
            for (int i = 1; i < 50; i++) {
                for (int x = 1; x < NumColumns; x++) {
                    addCellContent(bogusData[x], 1, 5, chineseFont, datatable);
                }
            }
            //添加分摊水费
            addCellContent("分摊水费", 2, 5, chineseFont, datatable);
            addCellContentLeft("在楼层-2F收费面积占比为36%；", 4, 5, chineseFont, datatable);
            addCellContent("109.00", 1, 5, chineseFont, datatable);
            addCellContent("1.30", 1, 5, chineseFont, datatable);
            addCellContent("141.70", 1, 5, chineseFont, datatable);
            addCellContent("2022.7.1-2022.7.31", 1, 5, chineseFont, datatable);
            //本月应缴水费
            addCellContent("本月应缴水费", 8, 5, chineseFont, datatable);
            addCellContent("921.70", 1, 5, chineseFont, datatable);
            addCellContent(" ", 1, 5, chineseFont, datatable);


            //本月管理费
            addCellContent("本月管理费", 2, 5, chineseFont, datatable);
            addCellContent("建筑面积：276.86平方米，管理费单价：17.5元/平方米", 6, 5, chineseFont, datatable);
            addCellContent("921.70", 1, 5, chineseFont, datatable);
            addCellContent("2022.7.1-2022.7.31", 1, 5, chineseFont, datatable);

            //本月空调费
            addCellContent("本月空调费", 2, 5, chineseFont, datatable);
            addCellContent("建筑面积：276.86平方米，管理费单价：17.5元/平方米", 6, 5, chineseFont, datatable);
            addCellContent("921.70", 1, 5, chineseFont, datatable);
            addCellContent("2022.7.1-2022.7.31", 1, 5, chineseFont, datatable);

            //空调加时费
            addCellContent("空调加时费", 2, 5, chineseFont, datatable);
            addCellContent("空调加时面积:6485.65㎡;收费标准:≤1000㎡，按200元/h;>1000㎡按照0.2元/㎡/h;", 6, 5, chineseFont, datatable);
            addCellContent("921.70", 1, 5, chineseFont, datatable);
            addCellContent("2022.7.1-2022.7.31", 1, 5, chineseFont, datatable);

            //合计
            addCellContentLeft("人民币合计应缴金额小写：", 8, 5, chineseFont, datatable);
            addCellContent("921.70", 1, 5, chineseFont, datatable);
            addCellContent(" ", 1, 5, chineseFont, datatable);
            addCellContentLeft("人民币合计应缴金额大写：贰万肆仟陆佰肆拾伍元肆角柒分：", 10, 5, chineseFont, datatable);
            //备注
            addCellContentLeft("备注：\n" +
                    "1.请在收到通知单后5个工作日内缴费，转账到以下账号：\n" +
                    "   开户行：XXXX\n" +
                    "   账户名：XXX\n" +
                    "   账号：XXXX\n" +
                    "2.请各租户转账时备注里注明为“012209”，以便财务查账。\n" +
                    "3.如需退租，请提前一个月通知物业工作人员\n" +
                    "4.如有疑问请致电物业管理中心咨询，电话：XXXX，我们将竭诚为您服务", 10, 5, chineseFont, datatable);

            document.add(datatable);
        } catch (Exception de) {
            de.printStackTrace();
        }
        // step5
        document.close();
    }

    private void addCellContent(String content, int colSpan, int padding, Font chineseFont, PdfPTable datatable) {
        Paragraph p = new Paragraph(content, chineseFont);
        PdfPCell cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPadding(padding);
        cell.setColspan(colSpan);
        datatable.addCell(cell);
    }

    private void addCellContentLeft(String content, int colSpan, int padding, Font chineseFont, PdfPTable datatable) {
        Paragraph p = new Paragraph(content, chineseFont);
        PdfPCell cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPadding(padding);
        cell.setColspan(colSpan);
        datatable.addCell(cell);
    }
}
